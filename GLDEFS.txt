HardwareShader PostProcess Scene
{
	Shader "Shaders/nitevis.fp" 330
	Name "NightVision"
	Uniform int exposure
}

brightmap sprite EYEZA0 { map "brightmaps/EYEZA0.png" }
brightmap sprite EYEZB0 { map "brightmaps/EYEZB0.png" }
brightmap sprite EYEZC0 { map "brightmaps/EYEZC0.png" }

brightmap sprite ARM1B0 { map "brightmaps/ARM1B0.png" }
brightmap sprite ARM1C0 { map "brightmaps/ARM1C0.png" }
brightmap sprite ARM1D0 { map "brightmaps/ARM1D0.png" }

brightmap sprite IVISA0 { map "brightmaps/IVISA0.png" }
brightmap sprite IVISB0 { map "brightmaps/IVISA0.png" }
brightmap sprite IVISC0 { map "brightmaps/IVISA0.png" }
brightmap sprite IVISD0 { map "brightmaps/IVISA0.png" }

brightmap sprite BAMOA0 { map "brightmaps/BAMOA0.png" }
brightmap sprite RVMSA0 { map "brightmaps/RVMSA0.png" }

brightmap sprite JBOTA0 { map "brightmaps/JBOTA0.png" }
brightmap sprite JBOTB0 { map "brightmaps/JBOTA0.png" }
brightmap sprite JBOTC0 { map "brightmaps/JBOTA0.png" }

brightmap sprite BON1A0 { map "brightmaps/BON1A0.png" }
brightmap sprite BON1B0 { map "brightmaps/BON1B0.png" }
brightmap sprite BON1C0 { map "brightmaps/BON1C0.png" }
brightmap sprite BON1D0 { map "brightmaps/BON1D0.png" }

brightmap sprite ARSDA0 { map "brightmaps/ARSDD0.png" }
brightmap sprite ARSDB0 { map "brightmaps/ARSDD0.png" }
brightmap sprite ARSDC0 { map "brightmaps/ARSDD0.png" }
brightmap sprite ARSDD0 { map "brightmaps/ARSDD0.png" }

brightmap sprite SRVOA0 { map "brightmaps/SRVOA0.png" }
brightmap sprite SRVOB0 { map "brightmaps/SRVOA0.png" }
brightmap sprite SRVOC0 { map "brightmaps/SRVOA0.png" }
brightmap sprite SRVOD0 { map "brightmaps/SRVOD0.png" }

brightmap sprite BROKB0 { map "brightmaps/BROKB0.png" }

brightmap sprite REPRA0 { map "brightmaps/REPRA0.png" }
brightmap sprite REPRB0 { map "brightmaps/REPRA0.png" }
brightmap sprite REPRC0 { map "brightmaps/REPRA0.png" }
brightmap sprite REPRD0 { map "brightmaps/REPRA0.png" }
brightmap sprite REPRE0 { map "brightmaps/REPRA0.png" }
brightmap sprite REPRF0 { map "brightmaps/REPRA0.png" }

brightmap sprite RTARA0 { map "brightmaps/RTARA0.png" }
brightmap sprite RTARB0 { map "brightmaps/RTARA0.png" }
brightmap sprite RTARC0 { map "brightmaps/RTARA0.png" }
brightmap sprite RTARD0 { map "brightmaps/RTARA0.png" }
brightmap sprite RTARE0 { map "brightmaps/RTARA0.png" }
brightmap sprite RTARF0 { map "brightmaps/RTARA0.png" }
brightmap sprite RTARG0 { map "brightmaps/RTARA0.png" }
brightmap sprite RTARH0 { map "brightmaps/RTARA0.png" }
brightmap sprite RTARI0 { map "brightmaps/RTARA0.png" }

brightmap sprite ARMKA0 { map "brightmaps/ARMKA0.png" }
brightmap sprite ARMKB0 { map "brightmaps/ARMKB0.png" }
brightmap sprite ARMKC0 { map "brightmaps/ARMKC0.png" }
brightmap sprite ARMKD0 { map "brightmaps/ARMKD0.png" }

brightmap sprite RIOFA0 { map "brightmaps/RIOFA0.png" }
brightmap sprite LMGFA0 { map "brightmaps/LMGFA0.png" }
