decal RevBulletHole
{
	pic BulHole
	x-scale 0.15
	y-scale 0.15
	randomflipx
	randomflipy
}

decal AymHole
{
	pic AzaHole
	x-scale 0.5
	y-scale 0.5
	randomflipx
	randomflipy
}

decal ScorchMark
{
	pic SCRCH2
	randomflipx
	randomflipy
	x-scale .9
	y-scale .9
	translucent 1
}

decal SmallScorch
{
	pic SCRCH2
	randomflipx
	randomflipy
	x-scale .6
	y-scale .6
	translucent 0.5
}

decal TinyScorch
{
	pic SCRCH2
	randomflipx
	randomflipy
	x-scale .25
	y-scale .25
	translucent 0.85
}

decal BigScorch
{
	pic SCRCH3
	randomflipx
	randomflipy
	x-scale .7
	y-scale .7
	translucent 0.6
}

decal HugeScorch
{
	pic SCRCH3
	randomflipx
	randomflipy
	x-scale 1.5
	y-scale 1.5
	translucent 0.85
}

decal MeleeCrack
{
	pic CRACK
	randomflipx
	randomflipy
	x-scale 0.5
	y-scale 0.5
	translucent 0.8
}
